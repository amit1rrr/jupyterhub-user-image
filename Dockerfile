# Pin jupyterlab to Version 0.34.0
FROM jupyter/base-notebook:8ccdfc1da8d5

ARG JUPYTERHUB_VERSION=0.9.*
RUN pip install --no-cache jupyterhub==$JUPYTERHUB_VERSION

# Install git so we can clone the nurtch demo notebook repo
USER root
RUN apt-get update && apt-get install -y git
# Switch back to notebook user (defined in the base image)
USER $NB_UID

# Install Rubix and other required packages on top of base nurtch image
RUN pip install --no-cache \
    rubix \
    python-gitlab \
    scipy \
    numpy \
    pandas \
    scikit-learn \
    matplotlib \
    tensorflow

# Install plotly extension for plotting metrics
RUN jupyter labextension install @jupyterlab/plotly-extension
